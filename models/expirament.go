package models

import (
	"fmt"
	"strings"

	"bitbucket.org/boolow5/kencon-api/db"
)

// Expirament is for testing code outputs
func Expirament() {
	user := User{
		Email:    "boolow5@gmail.com",
		Password: "helloworld",
		Profile: Profile{
			FirstName: "Mahdi",
			LastName:  "Bolow",
		},
	}

	fmt.Println("\n[Experiment] BEGIN :::::::::::")

	table := db.StructToTable(&user)
	fmt.Printf("Table:\n%s\n", table)

	query, values := db.StructToTableInsert(&user)

	strValues := []string{}
	for i := 0; i < len(values); i++ {
		strValues = append(strValues, fmt.Sprintf("'%v'", values[i]))
	}
	fmt.Printf("Insert Query:\n%s\nValues: %+v\n", query, strings.Join(strValues, ", "))

	fmt.Printf("SelectByIDQuery: \n%s\n", db.SelectByIDQuery(&user))

	fmt.Println("\n[Experiment] END :::::::::::\n")
}
