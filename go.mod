module bitbucket.org/boolow5/kencon-api

go 1.12

require (
	github.com/GeertJohan/go.rice v1.0.0 // indirect
	github.com/azer/debug v0.0.0-20141116004914-4769e572857f // indirect
	github.com/azer/go-style v0.0.0-20130627093536-14e31c5abbe5 // indirect
	github.com/codingsince1985/geo-golang v1.6.1
	github.com/gin-gonic/gin v1.4.0
	github.com/jinzhu/gorm v1.9.11
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/mroth/sseserver v1.1.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/skip2/go-qrcode v0.0.0-20190110000554-dc11ecdae0a9
	golang.org/x/crypto v0.0.0-20191002192127-34f69633bfdc
	gopkg.in/dgrijalva/jwt-go.v2 v2.7.0
	gopkg.in/mailgun/mailgun-go.v1 v1.1.1
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
