package db

import (
	"fmt"
	"reflect"
	"strings"

	"bitbucket.org/boolow5/kencon-api/config"
	"github.com/jmoiron/sqlx"

	// _ "github.com/mattn/go-sqlite3"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	// DB connection pool
	DB *sqlx.DB
)

// TableField represents a field that should be used to create SQL table or in insert
type TableField struct {
	Name     string
	DataType string
	Default  string
	IsNull   bool
	Value    interface{}
}

// ToTableField fields for a SQL table generation
func ToTableField(v reflect.Value, t reflect.StructField) TableField {
	tagInfo := t.Tag.Get("db")
	tagName := t.Tag.Get("json")

	isNull := false
	if len(strings.TrimSpace(tagName)) == 0 {
		tagName = t.Name
	}

	if strings.Contains(tagInfo, "null") || strings.Contains(tagInfo, "NULL") {
		tagInfo = strings.ReplaceAll(tagInfo, ",null", "")
		tagInfo = strings.ReplaceAll(tagInfo, ", null", "")
		tagInfo = strings.ReplaceAll(tagInfo, "null,", "")

		tagInfo = strings.ReplaceAll(tagInfo, ",NULL", "")
		tagInfo = strings.ReplaceAll(tagInfo, ", NULL", "")
		tagInfo = strings.ReplaceAll(tagInfo, "NULL,", "")
		isNull = true
	}

	dtype := toDBType(fmt.Sprintf("%s", v.Type))

	var value interface{}
	if v.IsValid() {
		value = v
	}

	return TableField{
		Name:     tagName,
		DataType: dtype,
		IsNull:   isNull,
		Value:    value,
	}
}

// InitPostgres initialzes postgres database connection
func InitPostgres() {
	conf := config.Get()
	fmt.Printf("[SQL] connecting to database '%s'\n", conf.Postgres.DBName)
	var err error
	DB, err = sqlx.Connect("postgres", conf.Postgres.URL())
	if err != nil {
		panic("[SQL] ERROR: " + err.Error())
	}

	DB.SetMaxOpenConns(1000) // The default is 0 (unlimited)
	DB.SetMaxIdleConns(10)   // defaultMaxIdleConns = 2
	DB.SetConnMaxLifetime(0) // 0, connections are reused forever.
	fmt.Printf("[SQL] Connected successfully!\nStats: %+v\n", DB.Stats())
}

// GetFieldValues returns fields from struct as TableField slice
func GetFieldValues(item interface{}) []TableField {
	fields := make([]TableField, 0)
	var ifv reflect.Value
	var ift reflect.Type

	if reflect.TypeOf(item).Kind() == reflect.Ptr {
		ifv = reflect.Indirect(reflect.ValueOf(item))
		ift = ifv.Type()
	} else {
		ifv = reflect.ValueOf(item)
		ift = reflect.TypeOf(item)
	}

	fmt.Printf("[GetFieldValues] is nil %v\tifv: %v\tift: %v\n", item == nil, ifv.Kind(), ift.Kind())

	for i := 0; i < ift.NumField(); i++ {
		v := ifv.Field(i)

		if !v.CanInterface() {
			fmt.Println("Skipping field:", ift.Field(i).Name)
			continue
		}

		switch v.Kind() {
		case reflect.Struct:
			fmt.Println("\nParsing struct:", ift.Field(i).Name, "\n", ift.Field(i).Type)
			if fmt.Sprint(ift.Field(i).Type) == "time.Time" {
				fmt.Println("Is time Object .....................")
				tagInfo := ift.Field(i).Tag.Get("db")
				tagName := ift.Field(i).Tag.Get("json")

				if len(strings.TrimSpace(tagName)) == 0 {
					tagName = ift.Field(i).Name
				}

				isNull := strings.Contains(tagInfo, "null") || strings.Contains(tagInfo, "NULL")
				fields = append(fields, TableField{
					Name:     tagName,
					DataType: toDBType(fmt.Sprintf("%s", ifv.Field(i).Type())),
					IsNull:   isNull,
					Value:    ifv.Field(i),
				})
			} else {
				fields = append(fields, GetFieldValues(v.Interface())...)
			}
		default:
			fields = append(fields, ToTableField(v, ift.Field(i)))
		}
	}

	return fields
}

func toDBType(t string) string {
	switch t {
	case "int", "int8", "int16", "int32", "int64", "uint", "uint8", "uint16", "uint32", "uint64":
		return "NUMERIC"
	case "float", "float8", "float16", "float32":
		return "NUMERIC"
	case "float64":
		return "NUMERIC(24, 8)"
	case "time.Time":
		return "TIMESTAMP WITH TIME ZONE"
	default:
		return "text"
	}
}

// IsZeroType returns if a x has any value of zero
func IsZeroType(x interface{}) bool {
	return reflect.DeepEqual(x, reflect.Zero(reflect.TypeOf(x)).Interface())
}

// StructToTable creates SQL string to create a table for the passed interface
func StructToTable(v interface{}) string {
	tableName := strings.ToLower(reflect.TypeOf(v).Elem().Name())
	fields := GetFieldValues(v)
	innerFields := ""

	for _, field := range fields {
		extra := ""
		if field.IsNull {
			extra += " NULL"
		} else {
			extra += " NOT NULL"
		}
		if field.Default != "" {
			extra += fmt.Sprintf(" DEFAULT '%s'", field.Default)
		}
		innerFields += fmt.Sprintf(`
	%s %s %s,
		`, field.Name, field.DataType, extra)
	}
	return fmt.Sprintf(`
CREATE TABEL %s (
	%s
);
	`, tableName, innerFields)
}

// StructToTableInsert creates SQL string to insert data in the passed interface
func StructToTableInsert(v interface{}) (string, []interface{}) {
	tableName := strings.ToLower(reflect.TypeOf(v).Elem().Name())
	fields := GetFieldValues(v)
	fieldNames := []string{}
	fieldValues := []interface{}{}

	for _, field := range fields {
		fieldNames = append(fieldNames, field.Name)
		val := field.Value

		if IsZeroType(val) && field.Default != "" {
			val = field.Default
		}

		valStr := fmt.Sprintf("%v", val)

		if strings.Contains(valStr, "ObjectIdHex(") {
			fmt.Printf("\tFieldName: %s, valStr: %v\n", field.Name, valStr)
			valStr = valStr[:len(valStr)-2]
			valStr = strings.Replace(valStr, "ObjectIdHex(\"", "", -1)
		}

		fieldValues = append(fieldValues, valStr)
	}

	fieldPlaceHolders := []string{}
	for i := 0; i < len(fieldNames); i++ {
		fieldPlaceHolders = append(fieldPlaceHolders, "?")
	}

	return fmt.Sprintf(`
	INSERT INTO %s (%s) VALUES (%s)
	`, tableName, strings.Join(fieldNames, ", "), strings.Join(fieldPlaceHolders, ", ")), fieldValues
}

// SelectByIDQuery generates SQL for selecting stuct by id
func SelectByIDQuery(v interface{}) (query string) {
	fields := GetFieldValues(v)
	fieldNames := []string{}
	for _, field := range fields {
		fieldNames = append(fieldNames, field.Name)
	}
	return fmt.Sprintf(`SELECT %s FROM %s WHERE id = ?`, strings.Join(fieldNames, ", "), strings.ToLower(reflect.TypeOf(v).Elem().Name()))
}
