package main

import (
	"fmt"
	"os"

	"bitbucket.org/boolow5/kencon-api/config"
	"bitbucket.org/boolow5/kencon-api/db"
	"bitbucket.org/boolow5/kencon-api/logger"
	"bitbucket.org/boolow5/kencon-api/models"
	"bitbucket.org/boolow5/kencon-api/notifications"
	"bitbucket.org/boolow5/kencon-api/router"
)

func main() {
	logger.Init(logger.DebugLevel)
	models.Version = config.Get().Version
	fmt.Printf("Starting %s carpooling api...\n", os.Args[0])
	db.InitMgDB()
	db.InitPostgres()
	err := models.Init()
	if err != nil {
		panic(err)
	}
	models.Expirament()
	router := router.Init()
	conf := config.Get()
	go notifications.Start(conf.SSEPort)
	notifications.InitMailGun(conf.MailGun)
	router.Run(conf.Port)
}
