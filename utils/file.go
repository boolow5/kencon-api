package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// LoadJSONFile loads and unmarshalls json file to the output variable
func LoadJSONFile(fullPath string, output interface{}) error {
	data, err := ioutil.ReadFile(fullPath)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, output)
	if err != nil {
		return err
	}
	return nil
}

// SaveToJSONFile marshals and saves input to a json file
func SaveToJSONFile(fullPath string, input interface{}) error {
	data, err := json.Marshal(input)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(fullPath, data, os.FileMode(0666))
	if err != nil {
		return err
	}
	return nil
}

// CreateDirectories creates multi level directories
func CreateDirectories(dir string, levels ...string) error {
	fmt.Printf("CreateDirectories(dir: %s, levels: %+v) GID: %v, UID: %v\n", dir, strings.Join(levels, string(os.PathSeparator)), os.Getegid(), os.Getuid())
	path := dir
	for _, part := range levels {
		if strings.TrimSpace(part) != "" {
			// concat to the last level
			if err := os.Chown(path, os.Getuid(), os.Getgid()); err != nil {
				return err
			}
			path = filepath.Join(path, part)
			fmt.Println("creating:", path)
			// create each level
			err := os.Mkdir(path, 0622)
			if err == nil || os.IsExist(err) {
				continue
			} else if err != nil {
				return err
			}
			// if err = os.Chmod(path, os.ModePerm); err != nil {
			// 	return err
			// }
		}
	}
	return nil
}
