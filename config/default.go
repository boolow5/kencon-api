package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

var (
	config   Configuration
	confPath string
)

// Configuration holds all config data for the project
type Configuration struct {
	Postgres        Postgres      `json:"postgres"`
	Version         string        `json:"version"`
	MgDBName        string        `json:"MgDBName"`
	MgDBURI         string        `json:"MgDBURI"`
	JWTKey          string        `json:"JWTKey"`
	Port            string        `json:"Port"`
	SSEPort         string        `json:"SSEPort"`
	MapKey          string        `json:"MapKey"`
	Admin           Admin         `json:"admin"`
	MailGun         MailGunConfig `json:"MailGun"`
	StaticDirectory string        `json:"static_directory"`
	StaticDomain    string        `json:"static_domain"`
}

// Postgres hodls database connection url and other data
type Postgres struct {
	Host           string `json:"host"`
	Port           string `json:"port"`
	Password       string `json:"pass"`
	User           string `json:"user"`
	DBName         string `json:"dbname"`
	SSLModeEnabled bool   `json:"sslmode_enabled"`
}

// Admin is initial admin that will have full access to everything
type Admin struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

// MailGunConfig is used for connecting to mail gun API
type MailGunConfig struct {
	APIKey    string `json:"APIKey"`
	DomainKey string `json:"DomainKey"`
	Email     string `json:"Email"`
}

// URL formats and returns full URL for the database connection string
func (p Postgres) URL() string {
	sslMode := "disable"
	if p.SSLModeEnabled {
		sslMode = "require"
	}
	pass := p.Password
	if len(os.Getenv("KENCON_API_DB_PASS")) > 0 {
		pass = os.Getenv("KENCON_API_DB_PASS")
	}
	if len(pass) < 8 {
		panic(fmt.Sprintf("[SQL] ERROR password too short! length is %d but it should be atleast 8 characters long.", len(pass)))
	}
	return fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s", p.Host, p.Port, p.User, p.DBName, pass, sslMode)
}

func init() {
	if len(os.Args) > 1 {
		confPath = os.Args[1]
	}
	if err := Load(confPath); err != nil {
		fmt.Println("Cannot load config file reason:", err)
		panic("[Config] ERROR: " + err.Error())
	}
	fmt.Println("[Config] Initialized configuration.")
}

// Get returns config local variable
func Get() Configuration {
	return config
}

// Load reads the config data from the json file
func Load(confPath string) error {
	file, err := ioutil.ReadFile(filepath.Join(confPath, "config.json"))
	if err != nil {
		return err
	}
	return json.Unmarshal(file, &config)
}

// GetPath returns directory which contains the config.json file
func GetPath() string {
	return confPath
}

// GetStaticDirectory full path for static files
func GetStaticDirectory() string {
	if os.Getenv("QAADO_DEV") != "true" {
		return config.StaticDirectory
	}
	return "static"
}
